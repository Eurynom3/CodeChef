numberOfRound = int(input())
player1 = 0
player2 = 0
lead1 = 0
lead2 = 0
for round in range(numberOfRound):
    (playerScore1, playerScore2) = map(int, input().split(' '))
    if playerScore1 > playerScore2:
        diff = playerScore1 - playerScore2
        if lead1 < diff or lead1 == 0:
            lead1 = diff
    else:
        diff = playerScore2 - playerScore1
        if lead2 < diff or lead2 == 0:
            lead2 = diff
    del diff

if lead1 > lead2:
    print("1 "+str(lead1))
else:
    print("2 "+str(lead2))
