def recherche(total, menus, exposant):
    if total != 0:
        if 2 ** exposant > total:
            exposant -= 1
            return recherche(total, menus, exposant)
        else:
            total -= 2 ** exposant
            menus += 1
            return recherche(total, menus, exposant)

    return menus

numburOfTest = int(input())

for i in range(numburOfTest):
    argent = int(input())
    numberOfMenus = recherche(argent, 0, 11)
    print(numberOfMenus)


