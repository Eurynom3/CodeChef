def addition(string):
    total = 0
    for x in range(len(string)):
        total += int(string[x])
    return total

numberOfTest = int(input())

for i in range(numberOfTest):
    digits = input()
    print(addition(digits))


